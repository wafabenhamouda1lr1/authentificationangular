import { User } from './user.model';
import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders , HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import {  of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private AUTH_API = 'http://localhost:8000/inscription';
  private _loginUrl ='http://localhost:8000/api_check';
  
  constructor(private httpClient: HttpClient) { }
  register( user : User){
    const body : User = {
      
      //id :user.id,
      username : user.username,
      password: user.password,
      email: user.email,
      confirm_password: user.confirm_password,
      roles: user.roles,

    }
    return this.httpClient.post(this.AUTH_API, body);
  }

  //register (user : User): Observable<User>{
    //return this.httpClient.post<User>(this.AUTH_API, user, httpOptions).pipe(
      //tap((newUser: User)=>console.log('aded user w/ id=${newUser.id}')),
      //catchError(this.handleError<User>('register'))
      //);
 // } 

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  login(){
    return this.httpClient.get(this._loginUrl);
  }

}
