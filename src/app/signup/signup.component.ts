import { SignupService } from './signup.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {


  repeatPassword: string;
  submitted : boolean;
  passwordMatched : boolean;

  SingUpForm = new FormGroup({ 
    username: new FormControl('', Validators.required),
    password: new FormControl('',Validators.required),
    email: new FormControl('', Validators.required),
    confirm_password: new FormControl(''),
    roles: new FormControl(''),
  });



  constructor(

    private _signupService: SignupService,

  ) { 

    this.submitted = false;
    this.passwordMatched = true;
  }

  ngOnInit() {
  }


  
  register(){


    this.submitted = true;


    if (this.SingUpForm.valid){

      this.SingUpForm.controls.confirm_password.setValue('123456789');
      //this.SingUpForm.controls.roles.setValue('ROLE_USER');

      this._signupService.signup(this.SingUpForm.value).then(

        resp => {
          console.log(resp)
        },
        error => {
          console.log(error)
        }
        
      )

    }
    console.log(this.SingUpForm.value);
    console.log(this.repeatPassword)




  }


  CheckPassword(event){

    console.log(event);

    if ( this.SingUpForm.value.password !== event ){

        this.passwordMatched = false;

    } else {

      this.passwordMatched = true;

    }

  }

  get form (){

    return this.SingUpForm.controls

  }

}
