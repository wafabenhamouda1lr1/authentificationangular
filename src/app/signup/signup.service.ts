import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SignupService {

 

  api ='http://localhost:8000/inscription';
  constructor(
    private http: HttpClient,
  ) {
}


  signup(data){

    var form_data = new FormData();

    for (var key in data) {
      form_data.append(key, data[key]);
    }


      return new Promise(resolve => {
        this.http.post(this.api,form_data, {
          headers: {},
          observe: 'response'
        }).subscribe(resp => {
          resolve({data: resp.body, status : resp.status});
        })
      });
  
  
    
    
  }
}
