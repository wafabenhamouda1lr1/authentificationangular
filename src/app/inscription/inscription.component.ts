import { User } from './../user.model';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from './../auth.service';
import { NgForm } from '@angular/forms';

  @Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.sass']
})
export class InscriptionComponent implements OnInit {

  user : User;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
constructor(private service:AuthService) { }

ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.user = {
      username: '',
      password: '',
      email: '',
      confirm_password: '',
      roles: ''
    }
  }

  OnSubmit(form: NgForm) {
    this.service.register(form.value)
      .subscribe((data: any) => {
        if (data.Succeeded == true) {
          this.resetForm(form);
         // this.toastr.success('User registration successful');
        }
        else {
          
        }
         // ////this.toastr.error(data.Errors[0]);
      });
  }

}






  
