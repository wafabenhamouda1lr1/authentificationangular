import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

 

  api ='http://localhost:8000/login';
  constructor(
    private http: HttpClient,
  ) {
}


  login(data){


      return new Promise(resolve => {
        this.http.post(this.api,data, {
          headers: {'Content-Type': 'application/json' },
          observe: 'response'
        }).subscribe(resp => {
          resolve({data: resp.body, status : resp.status});
        })
      });
  
  
    
    
  }
}
