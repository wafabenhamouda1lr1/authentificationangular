import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {


  LoginForm = new FormGroup({ 
  email: new FormControl('', Validators.required),
  password: new FormControl('',Validators.required),

});
  constructor(private _loginService: LoginService,) { }
  ngOnInit() {
   
  }
login(){


  this._loginService.login(this.LoginForm.value).then(

    resp => {

      localStorage.setItem('token', resp['data']['token']);
      console.log(resp)
    },
    error => {
      console.log(error)
    })

}


  
  


}
